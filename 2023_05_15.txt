Taught Manoj to run seeds on Test Server
To Running Seeds on Test Server.
 1. Edit the files and comment/uncomment the required seeds lines
 2. Edit file using nano command
 3. $ nano <file_path>
    $ nano server/seeds/seeds.js
 4. When in nano editor. Use ctrl+O to save, Ctrl+x to quit
 5. after editting the file go to emrdev/server.
 6. $ sudo NODE_ENV="test" npm run seeds
 7. Note here "test" is the enviroment. in stage we do "stage" in production server we do "prod". We don't define it on our lcoal server

I've got the understanding of "express-status-monitor" module in nodejs application
Stared implementing in out application.

Checked for any other issue/implementation remaining in Verity.
Had discussion with Babul sir regarding how I am performing performnace testing.
 1. I'm running the server in Desktop and exposing 80 via NGROK.io
 2. Running the K6 in my laptop to hit the exposed IP we got from NGROK.io
 3. K6 is showing the client side results. like total time takes, total hits made etc.
 4. "express-status-monitor" in the Desktop is showing how the PC is handling the load.

Had another discussion with Babul sir as few metrics seems unclear.
 1. removed unclear metrics from the document
 2. checking applications like Pm2, clinic.js to test load
 3. all of them don't generate the report we required. They are showing the real time values.

Updated the Excell sheet for better report presentation
generated report for 3 Apis

/admin/getAllRoles
checks.........................: 99.19% ✓ 867      ✗ 7    
    data_received..................: 36 MB  191 kB/s
    data_sent......................: 254 kB 1.3 kB/s
    http_req_blocked...............: avg=10.29ms  min=189ns    med=691ns    max=1.25s    p(90)=52.8ms   p(95)=65.18ms 
    http_req_connecting............: avg=5.16ms   min=0s       med=0s       max=1.13s    p(90)=21.7ms   p(95)=28.88ms 
    http_req_duration..............: avg=187.99ms min=22.92ms  med=168.73ms max=1.05s    p(90)=248.49ms p(95)=336.13ms
      { expected_response:true }...: avg=189.03ms min=109.94ms med=169.02ms max=1.05s    p(90)=248.93ms p(95)=336.19ms
    http_req_failed................: 0.80%  ✓ 7        ✗ 867  
    http_req_receiving.............: avg=68.14ms  min=71.96µs  med=60.14ms  max=985.15ms p(90)=99.92ms  p(95)=149.17ms
    http_req_sending...............: avg=108.5µs  min=26µs     med=109.14µs max=240.16µs p(90)=136.48µs p(95)=148.63µs
    http_req_tls_handshaking.......: avg=4.94ms   min=0s       med=0s       max=290.22ms p(90)=29.65ms  p(95)=35.89ms 
    http_req_waiting...............: avg=119.74ms min=22.67ms  med=109.53ms max=570.35ms p(90)=167.66ms p(95)=205.33ms
    http_reqs......................: 874    4.636033/s
    iteration_duration.............: avg=10.48s   min=10.02s   med=10.18s   max=13.55s   p(90)=12.35s   p(95)=12.41s  
    iterations.....................: 774    4.105594/s
    vus............................: 1      min=1      max=100
    vus_max........................: 100    min=100    max=100

/admin/getAllUsers
checks.........................: 98.75% ✓ 870      ✗ 11   
     data_received..................: 1.1 MB 5.8 kB/s
     data_sent......................: 243 kB 1.3 kB/s
     http_req_blocked...............: avg=8.84ms   min=240ns   med=697ns    max=313.69ms p(90)=54.92ms  p(95)=71.29ms 
     http_req_connecting............: avg=3.73ms   min=0s      med=0s       max=84.9ms   p(90)=22.64ms  p(95)=29.9ms  
     http_req_duration..............: avg=97.94ms  min=21.14ms med=82.29ms  max=568.15ms p(90)=157.23ms p(95)=178.01ms
       { expected_response:true }...: avg=98.83ms  min=55.48ms med=82.66ms  max=568.15ms p(90)=157.3ms  p(95)=178.05ms
     http_req_failed................: 1.24%  ✓ 11       ✗ 870  
     http_req_receiving.............: avg=743.13µs min=48.31µs med=137.44µs max=106.17ms p(90)=965.13µs p(95)=1.74ms  
     http_req_sending...............: avg=114.31µs min=38.38µs med=114.84µs max=271.36µs p(90)=135.97µs p(95)=143.3µs 
     http_req_tls_handshaking.......: avg=5.05ms   min=0s      med=0s       max=287.51ms p(90)=30.4ms   p(95)=39.9ms  
     http_req_waiting...............: avg=97.09ms  min=20.85ms med=81.71ms  max=567.93ms p(90)=154.7ms  p(95)=175.22ms
     http_reqs......................: 881    4.662122/s
     iteration_duration.............: avg=10.37s   min=10.02s  med=10.08s   max=12.58s   p(90)=12.27s   p(95)=12.31s  
     iterations.....................: 781    4.132937/s
     vus............................: 2      min=1      max=100
     vus_max........................: 100    min=100    max=100


/admin/physician-list
checks.........................: 99.77% ✓ 877      ✗ 2    
     data_received..................: 4.3 MB 23 kB/s
     data_sent......................: 227 kB 1.2 kB/s
     http_req_blocked...............: avg=8.73ms   min=194ns   med=699ns    max=358.69ms p(90)=51.26ms  p(95)=67.48ms 
     http_req_connecting............: avg=3.45ms   min=0s      med=0s       max=64.31ms  p(90)=20.98ms  p(95)=29.76ms 
     http_req_duration..............: avg=127.45ms min=21.22ms med=121.85ms max=558.64ms p(90)=164.93ms p(95)=191.33ms
       { expected_response:true }...: avg=127.68ms min=76.74ms med=121.88ms max=558.64ms p(90)=164.95ms p(95)=191.54ms
     http_req_failed................: 0.22%  ✓ 2        ✗ 877  
     http_req_receiving.............: avg=4.08ms   min=68.06µs med=1.07ms   max=422.48ms p(90)=3.32ms   p(95)=6.41ms  
     http_req_sending...............: avg=112.93µs min=28.31µs med=113.69µs max=237.8µs  p(90)=135µs    p(95)=142.39µs
     http_req_tls_handshaking.......: avg=5.19ms   min=0s      med=0s       max=320.41ms p(90)=29.08ms  p(95)=37.4ms  
     http_req_waiting...............: avg=123.25ms min=20.95ms med=119.54ms max=372.95ms p(90)=158ms    p(95)=177.73ms
     http_reqs......................: 879    4.625887/s
     iteration_duration.............: avg=10.41s   min=10.02s  med=10.12s   max=12.66s   p(90)=12.3s    p(95)=12.34s  
     iterations.....................: 779    4.09962/s
     vus............................: 1      min=1      max=100
     vus_max........................: 100    min=100    max=100

