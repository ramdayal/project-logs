Checking If All fields required by the client are getting displayed

Doubts
    There are multiple FT1 segments in the DFT file provided.
    Do we need to do it like this.
    Because I have already modified the code to generate multiple DFT for multiple Procedure code.

    I think instead of Id of user, NPI is used is the DFT file.

    In the ADT file named "F27305U0004080.TXT" in the remote PC 
    KAPA^ARFANELLA PT^KAMI^^^^225100000X^1245583657
    here "KAPA" is Id and "1245583657" is NPI, right?

    In the sample DFT file we were provided in the email
    1326629742^Bizzak^Kathryn^^^^^NPI
    here "1326629742" looks like NPI. "NPI" at the end is hardcoded

    or maybe the 1st field stores the data that is shown by the last field.
    Since last field says NPI, therefore the 1st field should be the NPI number of the person
    Im only assuming. Please clairify if I'm wrong.

Worked with lighhouse extension
Scores are: { performance: 66, Accessablity: 98, Best Practices: 92 }

Installed K6 in my PC
Working on writing scripts for out application for load testing
