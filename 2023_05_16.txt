
facing error
error code 1429

Anjali ma'am and Babul sir.
While testing I found few request we giving error on client side. But there were no server-side errors.
I debugged it and found error code was 1429 in k6
In K6 error 14xx corressponds to error 4xx
And error 429 is "Too Many Requests"
This is only coming when I'm using NGROK to host.
I am checking online for solution for this.
When using private IP on same WiFi network. No errors are coming

Modified K6 scripts to handle status code 429
when we receive status 429 we would wait for some time(3 seconds) before hitting again.

Helping Manoj with generating PDF and explaining how to test and work with it.

helped Rajat with re-rendering the child component

Edited the "express-status-monitor" to display max and average stats for CPU and memory usage

/admin/physician-list
checks.........................: 99.77% ✓ 877      ✗ 2    
    data_received..................: 4.3 MB 23 kB/s
    data_sent......................: 227 kB 1.2 kB/s
    http_req_blocked...............: avg=8.73ms   min=194ns   med=699ns    max=358.69ms p(90)=51.26ms  p(95)=67.48ms 
    http_req_connecting............: avg=3.45ms   min=0s      med=0s       max=64.31ms  p(90)=20.98ms  p(95)=29.76ms 
    http_req_duration..............: avg=127.45ms min=21.22ms med=121.85ms max=558.64ms p(90)=164.93ms p(95)=191.33ms
      { expected_response:true }...: avg=127.68ms min=76.74ms med=121.88ms max=558.64ms p(90)=164.95ms p(95)=191.54ms
    http_req_failed................: 0.22%  ✓ 2        ✗ 877  
    http_req_receiving.............: avg=4.08ms   min=68.06µs med=1.07ms   max=422.48ms p(90)=3.32ms   p(95)=6.41ms  
    http_req_sending...............: avg=112.93µs min=28.31µs med=113.69µs max=237.8µs  p(90)=135µs    p(95)=142.39µs
    http_req_tls_handshaking.......: avg=5.19ms   min=0s      med=0s       max=320.41ms p(90)=29.08ms  p(95)=37.4ms  
    http_req_waiting...............: avg=123.25ms min=20.95ms med=119.54ms max=372.95ms p(90)=158ms    p(95)=177.73ms
    http_reqs......................: 879    4.625887/s
    iteration_duration.............: avg=10.41s   min=10.02s  med=10.12s   max=12.66s   p(90)=12.3s    p(95)=12.34s  
    iterations.....................: 779    4.09962/s
    vus............................: 1      min=1      max=100
    vus_max........................: 100    min=100    max=100

 / admin / getKpiData
checks.........................: 98.69% ✓ 831      ✗ 11   
     data_received..................: 3.4 MB 18 kB/s
     data_sent......................: 190 kB 1.0 kB/s
     http_req_blocked...............: avg=13.36ms  min=248ns   med=733ns    max=1.59s    p(90)=54.07ms  p(95)=61.93ms 
     http_req_connecting............: avg=6.5ms    min=0s      med=0s       max=1.03s    p(90)=22.25ms  p(95)=25.2ms  
     http_req_duration..............: avg=96.36ms  min=23.2ms  med=70.71ms  max=741.31ms p(90)=158.02ms p(95)=200.91ms
       { expected_response:true }...: avg=97.07ms  min=49.86ms med=71.04ms  max=741.31ms p(90)=158.19ms p(95)=202.39ms
     http_req_failed................: 1.30%  ✓ 11       ✗ 831  
     http_req_receiving.............: avg=2.94ms   min=42.63µs med=779.51µs max=256.43ms p(90)=2.08ms   p(95)=3.95ms  
     http_req_sending...............: avg=116.94µs min=51.68µs med=114.04µs max=355.67µs p(90)=141.02µs p(95)=151.97µs
     http_req_tls_handshaking.......: avg=6.75ms   min=0s      med=0s       max=566.54ms p(90)=30.3ms   p(95)=35.81ms 
     http_req_waiting...............: avg=93.3ms   min=22.96ms med=69.33ms  max=696.44ms p(90)=156.04ms p(95)=191.7ms 
     http_reqs......................: 842    4.448696/s
     iteration_duration.............: avg=10.93s   min=10.02s  med=10.07s   max=17.77s   p(90)=16.26s   p(95)=16.33s  
     iterations.....................: 742    3.920348/s
     vus............................: 1      min=1      max=100
     vus_max........................: 100    min=100    max=100

 / admin / masterData  
checks.........................: 100.00% ✓ 825      ✗ 0    
     data_received..................: 1.7 MB  9.1 kB/s
     data_sent......................: 214 kB  1.1 kB/s
     http_req_blocked...............: avg=18.59ms  min=217ns   med=739ns    max=1.31s    p(90)=60.83ms  p(95)=72.36ms 
     http_req_connecting............: avg=10.91ms  min=0s      med=0s       max=1.17s    p(90)=25.19ms  p(95)=29.38ms 
     http_req_duration..............: avg=135.44ms min=20.29ms med=79.52ms  max=1.25s    p(90)=262.66ms p(95)=374.23ms
       { expected_response:true }...: avg=140.18ms min=50.56ms med=81.11ms  max=1.25s    p(90)=272.37ms p(95)=380.08ms
     http_req_failed................: 5.82%   ✓ 51       ✗ 825  
     http_req_receiving.............: avg=1.37ms   min=37.66µs med=131.24µs max=259.26ms p(90)=978.23µs p(95)=2.67ms  
     http_req_sending...............: avg=116.21µs min=42.62µs med=115.31µs max=308.53µs p(90)=143.85µs p(95)=155.93µs
     http_req_tls_handshaking.......: avg=7.5ms    min=0s      med=0s       max=342.11ms p(90)=33.67ms  p(95)=39.92ms 
     http_req_waiting...............: avg=133.95ms min=20.03ms med=78.77ms  max=1.25s    p(90)=262.15ms p(95)=367.74ms
     http_reqs......................: 876     4.649342/s
     iteration_duration.............: avg=11.22s   min=10.05s  med=10.08s   max=19.56s   p(90)=16.29s   p(95)=16.39s  
     iterations.....................: 725     3.847914/s
     vus............................: 1       min=1      max=100
     vus_max........................: 100     min=100    max=100
  CPU-> 29%
  MEM-> 93 MB


 / patient / patients-list
checks.........................: 99.86% ✓ 736      ✗ 1    
     data_received..................: 219 MB 1.2 MB/s
     data_sent......................: 842 kB 4.4 kB/s
     http_req_blocked...............: avg=25.99ms  min=184ns    med=739ns    max=1.15s   p(90)=73.58ms  p(95)=108.01ms
     http_req_connecting............: avg=12.3ms   min=0s       med=0s       max=1.06s   p(90)=30.84ms  p(95)=49.39ms 
     http_req_duration..............: avg=1.52s    min=321.43µs med=477.7ms  max=21.64s  p(90)=3.59s    p(95)=5.54s   
       { expected_response:true }...: avg=1.6s     min=51.01ms  med=675.8ms  max=21.64s  p(90)=3.72s    p(95)=5.78s   
     http_req_failed................: 5.64%  ✓ 44       ✗ 736  
     http_req_receiving.............: avg=965.01ms min=0s       med=502.95µs max=19.7s   p(90)=2.53s    p(95)=4.51s   
     http_req_sending...............: avg=240.38µs min=26.51µs  med=109.23µs max=98.87ms p(90)=150.03µs p(95)=167.97µs
     http_req_tls_handshaking.......: avg=16.48ms  min=0s       med=0s       max=1.24s   p(90)=39.23ms  p(95)=54.02ms 
     http_req_waiting...............: avg=556.61ms min=146.1µs  med=443.21ms max=2.96s   p(90)=1.29s    p(95)=1.71s   
     http_reqs......................: 780    4.116394/s
     iteration_duration.............: avg=13s      min=7.16s    med=11.73s   max=30.35s  p(90)=17.6s    p(95)=19.17s  
     iterations.....................: 636    3.356444/s
     vus............................: 3      min=1      max=100
     vus_max........................: 100    min=100    max=100


/ patient / discipline 
checks.........................: 99.63% ✓ 544      ✗ 2    
     data_received..................: 174 MB 886 kB/s
     data_sent......................: 669 kB 3.4 kB/s
     http_req_blocked...............: avg=79.25ms  min=152ns    med=540ns    max=2.2s     p(90)=214.42ms p(95)=459.25ms
     http_req_connecting............: avg=34.64ms  min=0s       med=0s       max=2.11s    p(90)=61.77ms  p(95)=132.1ms 
     http_req_duration..............: avg=7.28s    min=157.09µs med=5.95s    max=30.15s   p(90)=16.56s   p(95)=18.77s  
       { expected_response:true }...: avg=7.8s     min=120.43ms med=6.52s    max=30.15s   p(90)=16.76s   p(95)=18.93s  
     http_req_failed................: 6.84%  ✓ 40       ✗ 544  
     http_req_receiving.............: avg=6.86s    min=0s       med=5.45s    max=29.8s    p(90)=15.97s   p(95)=18.32s  
     http_req_sending...............: avg=1.64ms   min=24.02µs  med=72.71µs  max=585.36ms p(90)=150.01µs p(95)=169.8µs 
     http_req_tls_handshaking.......: avg=47.12ms  min=0s       med=0s       max=1.13s    p(90)=136.08ms p(95)=311.49ms
     http_req_waiting...............: avg=416.49ms min=60.83µs  med=284.56ms max=2.16s    p(90)=873.28ms p(95)=1.16s   
     http_reqs......................: 584    2.967147/s
     iteration_duration.............: avg=25.52s   min=7.22s    med=24.38s   max=58.55s   p(90)=39.31s   p(95)=42.23s  
     iterations.....................: 337    1.712206/s
     vus............................: 1      min=1      max=100
     vus_max........................: 100    min=100    max=100

/ patient / discipline but with IP on same network
checks.........................: 100.00% ✓ 566      ✗ 0    
     data_received..................: 201 MB  1.1 MB/s
     data_sent......................: 236 kB  1.2 kB/s
     http_req_blocked...............: avg=10.12ms min=1.93µs  med=4.78µs   max=250.28ms p(90)=46.25ms  p(95)=77.65ms 
     http_req_connecting............: avg=10.1ms  min=0s      med=0s       max=250.14ms p(90)=46.15ms  p(95)=77.55ms 
     http_req_duration..............: avg=7.18s   min=65.16ms med=4.54s    max=42.58s   p(90)=20.03s   p(95)=22.66s  
       { expected_response:true }...: avg=7.18s   min=65.16ms med=4.54s    max=42.58s   p(90)=20.03s   p(95)=22.66s  
     http_req_failed................: 0.00%   ✓ 0        ✗ 566  
     http_req_receiving.............: avg=7.06s   min=46.42µs med=4.41s    max=42.36s   p(90)=19.86s   p(95)=22.49s  
     http_req_sending...............: avg=42µs    min=7.99µs  med=27.96µs  max=208.35µs p(90)=102.72µs p(95)=118.96µs
     http_req_tls_handshaking.......: avg=0s      min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=124.2ms min=25.7ms  med=111.72ms max=653.44ms p(90)=208.53ms p(95)=234.31ms
     http_reqs......................: 566     2.991404/s
     iteration_duration.............: avg=23.02s  min=10.23s  med=22.15s   max=56.09s   p(90)=34.97s   p(95)=40.95s  
     iterations.....................: 357     1.886804/s
     vus............................: 1       min=1      max=100
     vus_max........................: 100     min=100    max=100


 / patient / episodes-by-patientId
     checks.........................: 99.82% ✓ 577      ✗ 1    
     data_received..................: 110 MB 568 kB/s
     data_sent......................: 488 kB 2.5 kB/s
     http_req_blocked...............: avg=137.56ms min=203ns    med=715ns    max=8.19s    p(90)=155.46ms p(95)=421.86ms
     http_req_connecting............: avg=104.79ms min=0s       med=0s       max=8.14s    p(90)=41.99ms  p(95)=81.62ms 
     http_req_duration..............: avg=4.51s    min=0s       med=3.36s    max=28.91s   p(90)=10.84s   p(95)=14.46s  
       { expected_response:true }...: avg=4.82s    min=123.05ms med=3.8s     max=28.91s   p(90)=11.42s   p(95)=14.76s  
     http_req_failed................: 6.63%  ✓ 41       ✗ 577  
     http_req_receiving.............: avg=3.84s    min=0s       med=2.73s    max=28.73s   p(90)=9.63s    p(95)=13.56s  
     http_req_sending...............: avg=378.15µs min=0s       med=106.87µs max=165.52ms p(90)=153.09µs p(95)=168.4µs 
     http_req_tls_handshaking.......: avg=34.67ms  min=0s       med=0s       max=1.3s     p(90)=85.7ms   p(95)=168.69ms
     http_req_waiting...............: avg=669.77ms min=0s       med=192.58ms max=11.09s   p(90)=776.78ms p(95)=4.49s   
     http_reqs......................: 618    3.204443/s
     iteration_duration.............: avg=17.51s   min=7.33s    med=15.68s   max=41.12s   p(90)=25.71s   p(95)=29.68s  
     iterations.....................: 478    2.478517/s
     vus............................: 2      min=1      max=100
     vus_max........................: 100    min=100    max=100

same but with localhost
checks.........................: 99.03% ✓ 411      ✗ 4    
     data_received..................: 70 MB  365 kB/s
     data_sent......................: 159 kB 824 B/s
     http_req_blocked...............: avg=55.71ms  min=3.35µs   med=7.19µs   max=1.16s    p(90)=174.18ms p(95)=303.52ms
     http_req_connecting............: avg=55.68ms  min=0s       med=0s       max=1.16s    p(90)=174.07ms p(95)=303.41ms
     http_req_duration..............: avg=12.14s   min=71.84ms  med=8.86s    max=1m0s     p(90)=26.83s   p(95)=33.17s  
       { expected_response:true }...: avg=11.68s   min=71.84ms  med=8.78s    max=57.35s   p(90)=26.48s   p(95)=32.37s  
     http_req_failed................: 0.96%  ✓ 4        ✗ 411  
     http_req_receiving.............: avg=11.89s   min=102.57µs med=8.69s    max=59.89s   p(90)=26.63s   p(95)=33.08s  
     http_req_sending...............: avg=58.22µs  min=17.36µs  med=41.02µs  max=246.66µs p(90)=113.29µs p(95)=126.8µs 
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=251.72ms min=22.71ms  med=173.69ms max=1.43s    p(90)=519.07ms p(95)=716.14ms
     http_reqs......................: 415    2.151569/s
     iteration_duration.............: avg=27.75s   min=10.25s   med=25.75s   max=1m16s    p(90)=41.74s   p(95)=50.07s  
     iterations.....................: 309    1.602011/s
     vus............................: 2      min=1      max=100
     vus_max........................: 100    min=100    max=100
This is also same butwith 30 users
     checks.........................: 100.00% ✓ 218      ✗ 0   
     data_received..................: 43 MB   328 kB/s
     data_sent......................: 87 kB   672 B/s
     http_req_blocked...............: avg=11.85ms min=3.86µs   med=6.88µs   max=1.03s    p(90)=13.18ms  p(95)=43.54ms 
     http_req_connecting............: avg=11.83ms min=0s       med=0s       max=1.03s    p(90)=13.08ms  p(95)=43.44ms 
     http_req_duration..............: avg=1.82s   min=37.45ms  med=658.74ms max=17.3s    p(90)=5.21s    p(95)=7.81s   
       { expected_response:true }...: avg=1.82s   min=37.45ms  med=658.74ms max=17.3s    p(90)=5.21s    p(95)=7.81s   
     http_req_failed................: 0.00%   ✓ 0        ✗ 218 
     http_req_receiving.............: avg=1.72s   min=102.48µs med=584.27ms max=17.04s   p(90)=4.85s    p(95)=7.73s   
     http_req_sending...............: avg=52.31µs min=19.38µs  med=39.72µs  max=179.17µs p(90)=102.62µs p(95)=128.36µs
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=105.4ms min=14.44ms  med=79.3ms   max=956.83ms p(90)=202.67ms p(95)=272.05ms
     http_reqs......................: 218     1.676348/s
     iteration_duration.............: avg=13.09s  min=10.06s   med=11.77s   max=27.3s    p(90)=17.02s   p(95)=18.68s  
     iterations.....................: 188     1.445658/s
     vus............................: 1       min=1      max=30
     vus_max........................: 30      min=30     max=30


financial class
 / common / financial-class
     checks.........................: 100.00% ✓ 848      ✗ 0    
     data_received..................: 2.7 MB  14 kB/s
     data_sent......................: 327 kB  1.7 kB/s
     http_req_blocked...............: avg=4.09ms  min=2.89µs  med=6.87µs  max=1.04s    p(90)=4.18ms   p(95)=14.03ms 
     http_req_connecting............: avg=4.07ms  min=0s      med=0s      max=1.04s    p(90)=4.07ms   p(95)=13.93ms 
     http_req_duration..............: avg=77.65ms min=5.06ms  med=43.55ms max=780.54ms p(90)=180.74ms p(95)=283.62ms
       { expected_response:true }...: avg=77.65ms min=5.06ms  med=43.55ms max=780.54ms p(90)=180.74ms p(95)=283.62ms
     http_req_failed................: 0.00%   ✓ 0        ✗ 848  
     http_req_receiving.............: avg=12.62ms min=48.63µs med=2.12ms  max=518.13ms p(90)=28.33ms  p(95)=54.47ms 
     http_req_sending...............: avg=47.75µs min=16.61µs med=38.55µs max=266.44µs p(90)=86.06µs  p(95)=117.91µs
     http_req_tls_handshaking.......: avg=0s      min=0s      med=0s      max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=64.98ms min=3.99ms  med=34.43ms max=655.49ms p(90)=138.62ms p(95)=230.82ms
     http_reqs......................: 848     4.490158/s
     iteration_duration.............: avg=10.89s  min=10s     med=10.05s  max=17.96s   p(90)=16.11s   p(95)=16.17s  
     iterations.....................: 748     3.960658/s
     vus............................: 1       min=1      max=100
     vus_max........................: 100     min=100    max=100

facilities
 / common / facilities-by-user
     checks.........................: 100.00% ✓ 835      ✗ 0    
     data_received..................: 74 MB   390 kB/s
     data_sent......................: 347 kB  1.8 kB/s
     http_req_blocked...............: avg=5.54ms   min=2.69µs  med=6.68µs   max=1.03s    p(90)=2.47ms   p(95)=7.7ms   
     http_req_connecting............: avg=5.52ms   min=0s      med=0s       max=1.03s    p(90)=2.36ms   p(95)=7.58ms  
     http_req_duration..............: avg=187.46ms min=52.14ms med=129.73ms max=1.24s    p(90)=383.7ms  p(95)=465.12ms
       { expected_response:true }...: avg=187.46ms min=52.14ms med=129.73ms max=1.24s    p(90)=383.7ms  p(95)=465.12ms
     http_req_failed................: 0.00%   ✓ 0        ✗ 835  
     http_req_receiving.............: avg=127.21ms min=71.16µs med=82.32ms  max=1.07s    p(90)=314.44ms p(95)=399.93ms
     http_req_sending...............: avg=46.2µs   min=12.19µs med=38.36µs  max=239.89µs p(90)=80.58µs  p(95)=110.18µs
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=60.2ms   min=9.85ms  med=43.61ms  max=413.46ms p(90)=114.53ms p(95)=167.71ms
     http_reqs......................: 835     4.423467/s
     iteration_duration.............: avg=11.03s   min=10.05s  med=10.16s   max=17.41s   p(90)=16.21s   p(95)=16.37s  
     iterations.....................: 735     3.89371/s
     vus............................: 1       min=1      max=100
     vus_max........................: 100     min=100    max=100

 / admin / physicians-by-name
       checks.........................: 100.00% ✓ 489      ✗ 0    
     data_received..................: 259 MB  1.4 MB/s
     data_sent......................: 195 kB  1.0 kB/s
     http_req_blocked...............: avg=30.49ms min=3.54µs   med=6.96µs   max=3.05s    p(90)=48.69ms  p(95)=81.04ms 
     http_req_connecting............: avg=30.46ms min=0s       med=0s       max=3.05s    p(90)=48.58ms  p(95)=80.9ms  
     http_req_duration..............: avg=9.03s   min=61.48ms  med=3.54s    max=32.68s   p(90)=28.81s   p(95)=30.2s   
       { expected_response:true }...: avg=9.03s   min=61.48ms  med=3.54s    max=32.68s   p(90)=28.81s   p(95)=30.2s   
     http_req_failed................: 0.00%   ✓ 0        ✗ 489  
     http_req_receiving.............: avg=8.88s   min=107.63µs med=3.41s    max=32.46s   p(90)=28.37s   p(95)=30.02s  
     http_req_sending...............: avg=54.79µs min=16.86µs  med=40.31µs  max=190.7µs  p(90)=109.23µs p(95)=123.32µs
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=151.9ms min=60.5ms   med=137.67ms max=844.01ms p(90)=223.35ms p(95)=284.18ms
     http_reqs......................: 489     2.564237/s
     iteration_duration.............: avg=22.22s  min=10.31s   med=19.68s   max=47.9s    p(90)=39.55s   p(95)=40.84s  
     iterations.....................: 377     1.976927/s
     vus............................: 1       min=1      max=100
     vus_max........................: 100     min=100    max=100